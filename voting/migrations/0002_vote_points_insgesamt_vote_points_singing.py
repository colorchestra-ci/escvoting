# Generated by Django 4.2.7 on 2024-05-03 22:32

from django.db import migrations, models
import voting.models


class Migration(migrations.Migration):

    dependencies = [
        ('voting', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vote',
            name='points_insgesamt',
            field=models.IntegerField(default=0, validators=[voting.models.validate_esc_points]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='vote',
            name='points_singing',
            field=models.IntegerField(default=0, validators=[voting.models.validate_esc_points]),
            preserve_default=False,
        ),
    ]
